#!/usr/bin/env python3

from copy import deepcopy

def new_board():
    """ () -> 3x3 matrix of int

    Creates a new, empty tic tac toe board, which is a 3x3 int array
    where 0 means empty, 2 means X, and 2 means O

    >>> board = new_board()
    >>> board
    [[0, 0, 0], [0, 0, 0], [0, 0, 0]]

    """
    return [[0 for i in range(3)] for j in range(3)]


def representation(i):
    """ (int) -> char

    Returns the character representing the value for i

    >>> representation(0)
    ' '
    >>> representation(1)
    'X'
    >>> representation(2)
    'O'
    """

    if ( i == 0 ):
        return " "
    elif ( i == 1 ):
        return "X"
    elif ( i == 2 ):
        return "O"
    else:
        raise ValueError("Argument must be in 0, 1, or 2")
        
def print_board(board):
    """ (3x3 array of int) -> NULL

    prints the tic tac toe board to the screen, where board is a
    3x3 matrix of ints with 0 meaning empty, 1 meaning X, and 2 meaning O

    >>> print_board([[0, 0, 0], [0, 0, 0], [0, 0, 0]])
       |   |   
    ---+---+---
       |   |   
    ---+---+---
       |   |   

    >>> print_board([[1, 0, 2], [2, 1, 0], [0, 2, 1]])
     X |   | O 
    ---+---+---
     O | X |   
    ---+---+---
       | O | X 

    """

    for i in range(3):
        if ( i != 0 ):
            print("---+---+---")
            
        print(" {:1} | {:1} | {:1} ".format(
            representation(board[i][0]),
            representation(board[i][1]),
            representation(board[i][2])
            ))

def check_for_win(board):
    """ (3x3 of int) -> int

    Checks whether one party has won, returns 0 for no win, 1 for X win,
    2 for O win.

    >>> check_for_win([[0, 0, 0], [0, 0, 0], [0, 0, 0]])
    0
    >>> check_for_win([[1, 1, 1], [0, 0, 0], [0, 0, 0]])
    1
    >>> check_for_win([[0, 0, 0], [1, 1, 1], [0, 0, 0]])
    1
    >>> check_for_win([[0, 0, 0], [0, 0, 0], [1, 1, 1]])
    1
    >>> check_for_win([[1, 0, 0], [1, 0, 0], [1, 0, 0]])
    1
    >>> check_for_win([[0, 1, 0], [0, 1, 0], [0, 1, 0]])
    1
    >>> check_for_win([[0, 0, 1], [0, 0, 1], [0, 0, 1]])
    1
    >>> check_for_win([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
    1
    >>> check_for_win([[0, 0, 1], [0, 1, 0], [1, 0, 0]])
    1
    >>> check_for_win([[2, 2, 2], [0, 0, 0], [0, 0, 0]])
    2
    >>> check_for_win([[0, 0, 0], [2, 2, 2], [0, 0, 0]])
    2
    >>> check_for_win([[0, 0, 0], [0, 0, 0], [2, 2, 2]])
    2
    >>> check_for_win([[2, 0, 0], [2, 0, 0], [2, 0, 0]])
    2
    >>> check_for_win([[0, 2, 0], [0, 2, 0], [0, 2, 0]])
    2
    >>> check_for_win([[0, 0, 2], [0, 0, 2], [0, 0, 2]])
    2
    >>> check_for_win([[2, 0, 0], [0, 2, 0], [0, 0, 2]])
    2
    >>> check_for_win([[0, 0, 2], [0, 2, 0], [2, 0, 0]])
    2
    """

    for i in range(3):
        if 0 != board[i][0] == board[i][1] == board[i][2]:
            return board[i][0]
        if 0 != board[0][i] == board[1][i] == board[2][i]:
            return board[0][i]
    if 0 != board[0][0] == board[1][1] == board[2][2]:
        return board[0][0]
    if 0 != board[2][0] == board[1][1] == board[0][2]:
        return board[2][0]
    return 0
        
def check_for_full(board):
    """( 3x3 of int ) -> bool
    >>> check_for_full([[0, 0, 0], [0, 0, 0], [0, 0, 0]])
    False
    >>> check_for_full([[1, 1, 2], [1, 2, 2], [1, 2, 1]])
    True
    """

    for i in range(3):
        for j in range(3):
            if board[i][j] == 0:
                return False
    return True

def check_for_end(board):
    ## Check if someone has won
    winner = check_for_win( board )
    if winner > 0:
        print("Player {:1} has won. Congratulations".format(
            representation(winner)))
        return True
    ## Check if the board is full
    if check_for_full(board):
        print("It is a tie!")
        return True
    ## We're still playing...
    return False

def set_piece(board, player, row, col):
    """
    >>> board = new_board()
    >>> set_piece(board, 1, 0, 0)
    True
    >>> print_board(board)
     X |   |   
    ---+---+---
       |   |   
    ---+---+---
       |   |   
    >>> set_piece(board, 2, 0, 0)
    False
    >>> print_board(board)
     X |   |   
    ---+---+---
       |   |   
    ---+---+---
       |   |   
    >>> set_piece(board, 2, 1, 0)
    True
    >>> print_board(board)
     X |   |   
    ---+---+---
     O |   |   
    ---+---+---
       |   |   
    """
    if board[row][col] == 0:
        board[row][col] = player
        return True
    return False

def eval_choice(choice):
    """(num) -> int, int

    >>> eval_choice(1)
    (0, 2)
    >>> eval_choice(2)
    (1, 2)
    >>> eval_choice(3)
    (2, 2)
    >>> eval_choice(4)
    (0, 1)
    >>> eval_choice(5)
    (1, 1)
    >>> eval_choice(6)
    (2, 1)
    >>> eval_choice(7)
    (0, 0)
    >>> eval_choice(8)
    (1, 0)
    >>> eval_choice(9)
    (2, 0)
    """
    col = int((choice-1) % 3)
    row = 2 - int((choice-1)/3)

    return col, row

def check_for_win_positions(board, player):
    possible_positions = []
    for i in range(1, 10):
        b = deepcopy(board)
        col, row = eval_choice(i)
        if set_piece(b, player, row, col):
            if check_for_win(b) == player:
                possible_positions.append(i)
    return possible_positions

def play():
    board = new_board()
    player = 1
    print_board(board)
    while not check_for_end(board):
        choice = 0
        wins = check_for_win_positions(board, player)
        if len(wins) > 0:
            print("Auto-Win: {}".format(wins[0]))
            choice = wins[0]
        losses = check_for_win_positions(board, 3-player)
        if len(losses) > 0 and len(wins) == 0:
            print("Auto-Prevent-Loss: {}".format(losses[0]))
            choice = losses[0]
        while not choice in list(range(1, 10)):
            choice = int(input( "Player {:1} choice: ".format(representation(player))))
        col, row = eval_choice(choice)
        success = set_piece(board, player, row, col)
        if success:
            player = 3-player
            print_board(board)
        
if __name__ == '__main__':
    play()
##    import doctest
##    doctest.testmod()
    
