#!/usr/bin/env python3

class tictactoe():

    # The triplets in winning_rows contain the indices
    # of all possible winning locations.
    winning_rows = [
        [0, 1, 2], [3, 4, 5], [6, 7, 8],
        [0, 3, 6], [1, 4, 7], [2, 5, 8],
        [0, 4, 8], [2, 4, 6]
        ]

    # player representations. 0 is empty.
    player_rep = [" ", "X", "O"]

    player = 1
    map = [0] * 9

    def __init__(self, map=[0]*9):
        self.map = list(map)
        self.player = 1

    def next_player(self):
        """ () -> null

        Changes the player from 1 to 2 and vice versa.

        >>> b = tictactoe()
        >>> b.player = 1
        >>> b.next_player()
        >>> b.player
        2
        >>> b.next_player()
        >>> b.player
        1
        """
        assert self.player in [1, 2]
        self.player = 3 - self.player

    def set_piece(self, index):
        """ (int) -> bool

        Sets a piece of the active player to the position
        marked by index, if board is still empty there.

        Returns True if successful, False if not (eg. position
        was not free).

        >>> b = tictactoe()
        >>> b.player = 1
        >>> b.set_piece(0)
        True
        >>> b.map # after setting X to lower left
        [1, 0, 0, 0, 0, 0, 0, 0, 0]
        >>> b.player
        1
        >>> b.player = 2
        >>> b.set_piece(4)
        True
        >>> b.map # after setting O to middle
        [1, 0, 0, 0, 2, 0, 0, 0, 0]
        >>> b.set_piece(0) # position already taken by X
        False
        >>> b.map # after O tried to take position held by X
        [1, 0, 0, 0, 2, 0, 0, 0, 0]
        """

        if not index in list(range(9)):
            return IndexError()

        if self.map[index] == 0:
            self.map[index] = self.player
            return True

        return False

    def convert_input(n):
        """(int) -> int

        Converts the input from the number pad (1-9) into
        the index of the corresponding positions (0-8)

        >>> tictactoe.convert_input(1)
        0
        >>> tictactoe.convert_input(2)
        1
        >>> tictactoe.convert_input(3)
        2
        >>> tictactoe.convert_input(4)
        3
        >>> tictactoe.convert_input(5)
        4
        >>> tictactoe.convert_input(6)
        5
        >>> tictactoe.convert_input(7)
        6
        >>> tictactoe.convert_input(8)
        7
        >>> tictactoe.convert_input(9)
        8

        """

        if not n in list(range(1, 10)):
            raise ValueError("Expected input 1-9, got {}".format(n))
        return n-1
        
        
    def check_full(self):
        """ () -> bool

        returns True if and only if all positions of the
        board are taken (1 or 2)

        >>> b = tictactoe()
        >>> b.check_full()
        False
        >>> b.map = [1, 1, 2, 1, 2, 2, 1, 2, 1]
        >>> b.check_full() # full, no winner
        True
        >>> b.map = [1, 1, 2, 1, 2, 2, 0, 2, 1]
        >>> b.check_full() # Only one position sill free
        False
        >>> b.map = [1, 1, 1, 2, 2, 0, 0, 0, 0]
        >>> b.check_full() # win, but not full
        False

        """

        return not any( p == 0 for p in self.map)

    def check_win(self):
        """ () -> int

        returns 0 if no winner, player number if it noticed
        a winner. Assumes only one winner.

        >>> tictactoe.check_win(tictactoe())
        0
        >>> tictactoe.check_win(tictactoe([1, 1, 1, 0, 0, 0, 0, 0, 0]))
        1
        >>> tictactoe.check_win(tictactoe([0, 0, 2, 0, 2, 0, 2, 0, 0]))
        2
        >>> tictactoe.check_win(tictactoe([0, 1, 1, 1, 0, 0, 0, 0, 0]))
        0
        >>> tictactoe.check_win(tictactoe([2, 1, 2, 1, 2, 2, 1, 2, 1]))
        0
        """
        for possib in tictactoe.winning_rows:
            checkrow = [self.map[i] for i in possib]
            if 0 != checkrow[0] == checkrow[1] == checkrow[2]:
                return checkrow[0]
        return 0

    def check_end(self):
        """ () -> bool

        returns True if either the board is full (displaying a tie),
        or one player has won (printing the player)

        >>> tictactoe.check_end(tictactoe())
        False
        >>> tictactoe.check_end(tictactoe([1, 1, 1, 0, 0, 0, 0, 0, 0]))
        Player X wins
        True
        >>> tictactoe.check_end(tictactoe([2, 2, 2, 0, 0, 0, 0, 0, 0]))
        Player O wins
        True
        >>> tictactoe.check_end(tictactoe([2, 1, 2, 1, 2, 2, 1, 2, 1]))
        It is a tie
        True
        """
            
        winner = self.check_win()
        if winner > 0:
            print( "Player {} wins".format(tictactoe.player_rep[winner]))
            return True
        if self.check_full():
            print( "It is a tie" )
            return True
        return False

    def print_board(self):
        """ () -> null

        Prints out the board as it is.

        >>> tictactoe.print_board(tictactoe())
           |   |   
        ---+---+---
           |   |   
        ---+---+---
           |   |   
        >>> tictactoe.print_board(tictactoe([1, 2, 1, 2, 1, 0, 2, 0, 0]))
         O |   |   
        ---+---+---
         O | X |   
        ---+---+---
         X | O | X 
        """

        
        print(" {:1} | {:1} | {:1} ".format(
            tictactoe.player_rep[self.map[6]],
            tictactoe.player_rep[self.map[7]],
            tictactoe.player_rep[self.map[8]]))
        print("---+---+---")
        print(" {:1} | {:1} | {:1} ".format(
            tictactoe.player_rep[self.map[3]],
            tictactoe.player_rep[self.map[4]],
            tictactoe.player_rep[self.map[5]]))
        print("---+---+---")
        print(" {:1} | {:1} | {:1} ".format(
            tictactoe.player_rep[self.map[0]],
            tictactoe.player_rep[self.map[1]],
            tictactoe.player_rep[self.map[2]]))


    def get_board(self):
        return self.map

    def get_player(self):
        return tictactoe.player_rep[self.player]

    def check_chance(self, player):
        """ (int) -> int

        Checks whether there is a chance for the player to win with one
        move. If yes, returns one possible choice. Returns 0 if not.

        >>> game=tictactoe()
        >>> game.check_chance(1)
        0
        >>> game=tictactoe([1, 1, 0, 0, 0, 0, 0, 0, 0])
        >>> game.check_chance(1)
        3
        >>> game=tictactoe([0, 0, 2, 0, 0, 0, 0, 0, 2])
        >>> game.check_chance(2)
        6
        

        """

        for i in range(10):
            test_game = tictactoe(list(self.get_board()))
            test_game.player = player
            if test_game.set_piece(i):
                if test_game.check_win():
                    return i + 1

        return 0


    def play(self, autoplay=True):
        """ () -> int

        Play the game. Will repeatedly ask for number (1-9) and place a piece
        until someone wins or there is a tie, or someone entered 0

        """

        self.print_board()

        while not self.check_end():
            if autoplay:
                choice = self.check_chance(self.player)
                if choice > 0:
                    print("Autoplay for player {:1} for win: {:1}".format(
                        self.get_player(), choice
                        ))
                else:
                    choice = self.check_chance(3-self.player)
                    if choice > 0:
                        print("Autoplay for player {:1} against loss: {:1}".format(
                        self.get_player(), choice
                        ))
            if choice == 0 or not autoplay:
                choice = int(input(
                    "Player {:1} enter choice (0 for exit): ".format(
                        self.get_player()
                        )
                    ))
            if choice == 0:
                break
            if not choice in list(range(1,10)):
                continue
            index = tictactoe.convert_input(choice)
            if self.set_piece(index):
                self.next_player()
                self.print_board()

    def play_new(self):
        self.__init__()
        self.play()
                

if __name__ == '__main__':
##    game = tictactoe()
##    game.play()
    import doctest
    doctest.testmod()
